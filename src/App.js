import React, { useState } from "react";

/**
 * If count is 0 display "POOP_TEXT"
 * If count is divisible by 3 display "COUNTER_TEXT"
 * If count is divisible by 5 display "FOO_TEXT"
 * If count is divisible by 3 && 5 display "COUNTER_FOO_TEXT"
 * If count is not divisible by 3, 5 display "POOP_TEXT"
 * The increment button should increase the "count" variable
 * The decrement button should decrease the "count" variable
 * The "count" variable should never go below 0
 */

const COUNTER_TEXT = "Counter";
const FOO_TEXT = "Foo";
const COUNTER_FOO_TEXT = "🙅🏿‍♂️ Counter Foo 🙅🏿‍♂️";
const POOP_TEXT = "💩";

function App() {
  const [count, setCount] = useState(0);
  const [text, setText] = useState(POOP_TEXT);
  const textPicker = (cnt) => {
    if (cnt % 3 === 0 && cnt % 5 === 0) {
      setText(COUNTER_FOO_TEXT);
    } else if (cnt % 3 === 0) {
      setText(COUNTER_TEXT);
    } else if (cnt % 5 === 0) {
      setText(FOO_TEXT);
    } else {
      setText(POOP_TEXT);
    }
  };

  const handleInc = () => {
    setCount((cnt) => cnt + 1);
    textPicker(count);
    console.log(text);
  };

  const handleDec = () => {
    if (count > 0) {
      setCount((cnt) => cnt - 1);
      textPicker(count);
    }
  };

  const logCount = () => {
    console.log(count);
  };

  return (
    <div style={styles.app}>
      <h1>{count > 0 ? count - 1 : 0}</h1>
      <h3>{text}</h3>
      <div style={styles.buttons}>
        <button onClick={handleInc}>Increment</button>
        <button onClick={handleDec}>Decrement</button>
        <button onClick={logCount}>Check Count</button>
      </div>
    </div>
  );
}

const styles = {
  app: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
  },
};
export default App;
